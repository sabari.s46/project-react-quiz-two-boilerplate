import React, { Component } from "react";
import Data from "../resources/quizQuestions.json";
import "../App.css";

export default class Quiz extends Component {
  constructor() {
    super();
    this.state = {
      questions: Data,
      currentIndex: 0,
    };
  }

  handleNextQuestion = () => {
    if (this.state.currentIndex < this.state.questions.length - 1) {
      this.setState((prevState) => ({
        currentIndex: prevState.currentIndex + 1,
      }));
    }
  };

  handlePreviousQuestion = () => {
    if (this.state.currentIndex > 0) {
      this.setState((prevState) => ({
        currentIndex: prevState.currentIndex - 1,
      }));
    }
  };

  render() {
    // console.log(Data);
    const currentQuestion = this.state.questions[this.state.currentIndex];

    return (
      <div className="Content">
        <div className="Quiz_Content">
          <div className="QHeading flex_center">Question</div>
          <div className="Question_Count">{`${this.state.currentIndex + 1} of ${
            this.state.questions.length
          }`}</div>
          <div className="Question flex_center">{currentQuestion.question}</div>
          <div className="Options ">
            <div className="Option1">{currentQuestion.optionA}</div>
            <div className="Option2">{currentQuestion.optionB}</div>
            <div className="Option3">{currentQuestion.optionC}</div>
            <div className="Option4">{currentQuestion.optionD}</div>
          </div>
          <div className="Controls ">
            <div className="Control1" onClick={this.handlePreviousQuestion}>
              Previous
            </div>
            <div className="Control2" onClick={this.handleNextQuestion}>
              Next
            </div>
            <div
              className="Control3"
              onClick={() => prompt("Are you sure you want to quit ?")}
            >
              Quit
            </div>
          </div>
        </div>
      </div>
    );
  }
}
